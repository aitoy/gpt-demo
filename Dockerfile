FROM python:3.11

COPY ./requirements.txt .
RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple --no-cache-dir

WORKDIR /app
COPY . .

CMD ["python", "src/server.py"]
