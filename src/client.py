import asyncio
import websockets

async def send_receive():
    uri = "ws://localhost:8765"  # 替换为您的WebSocket服务器地址

    async with websockets.connect(uri) as websocket:
        while True:
            message = input("Enter a message to send: ")
            await websocket.send(message)
            print(f"Sent message: {message}")

            response = await websocket.recv()
            print(f"Received response: {response}")

            # 当收到特定消息后退出循环
            if response == "quit":
                break

asyncio.get_event_loop().run_until_complete(send_receive())
