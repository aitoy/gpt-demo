from revChatGPT.V1 import Chatbot as ChatbotV1
from revChatGPT.V3 import Chatbot as ChatbotV3
import os

access_token = os.environ.get("access_token")
api_key = os.environ.get("api_key")
system_prompt = os.environ.get("system_prompt")

chatbot_v1 = ChatbotV1(config={
  "access_token": access_token
})

chatbot_v3 = ChatbotV3(
    api_key=api_key,
    system_prompt = system_prompt,
    # proxy = "https://chatgpt-proxy.klec.top/proxy"
    )

def test_v1():
    print("ChatbotV1: ")
    # 与ChatGPT进行对话
    while True:
        user_input = input("You: ")
        if user_input.lower() == 'quit':
            break

        prev_text = ""
        for data in chatbot_v1.ask(user_input):
            message = data["message"][len(prev_text):]
            print(message, end="", flush=True)
            prev_text = data["message"]

        print()

def test_v3():
    print("ChatbotV3: ")
    # 与ChatGPT进行对话
    while True:
        user_input = input("You: ")
        if user_input.lower() == 'quit':
            break

        data = chatbot_v3.ask(user_input)
        print(data)

if __name__ == '__main__':
    test_v1()
    test_v3()
